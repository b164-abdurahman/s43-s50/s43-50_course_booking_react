//lets create a component that will be use to navigate aroung the app.

//1. Identify and prepare the materials that will be used to create this new component.
import { Navbar, Nav }  from 'react-bootstrap';

	//2. create a function that will describe the anatomy of the component.
	//3. this component should become usable for other modules in the app, we have to expose the data.
export default function AppNavbar() {
	return(
      <Navbar bg="light" expand="lg">
         <Navbar.Brand>
         	Course Booking App B164
         </Navbar.Brand>
         <Navbar.Toggle aria-controls="basic-navbar-nav"/>
         <Navbar.Collapse id="basic-navbar-nav">
          	<Nav className="ml-auto">
	            <Nav.Link> Register </Nav.Link>
	            <Nav.Link> Login </Nav.Link>
	            <Nav.Link> Courses</Nav.Link>      		
          	</Nav>
         </Navbar.Collapse>
      </Navbar>		
	)
};