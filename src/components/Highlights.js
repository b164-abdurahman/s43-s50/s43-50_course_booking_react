//lets create another reuable component, the purpose of this component is to be able to identify and display the features and services that our app will be able to provide.
  //Grid system(Row, Col) and Card component
  //this approach will be different from the method we did from the first 2 components.
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';

//create a function to describe the anatomy of the component.
//expose the data for it to become usable by our application

//our next goal is to make the component responsive as possible.
export default function Highlights(){
	return(
      <Row className="mt-5 mb-5">
   		  <Col xs={12} md={4}>
   		      {/*1st Card Component*/}
      		  <Card className="p-3">
      		  	  <Card.Body>
      		  	  	 <Card.Title>Learn From Home</Card.Title>
      		  	  	 <Card.Text>
      		  	  	 	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci maiores quasi similique aliquam sint amet!
      		  	  	 </Card.Text>
      		  	  </Card.Body>
      		  </Card>	
   		  </Col>

   		  <Col xs={12} md={4}>
      		  {/*2nd Card Component*/}
      		  <Card className="p-3">
      		  	  <Card.Body>
      		  	  	 <Card.Title>Learn From Home</Card.Title>
      		  	  	 <Card.Text>
      		  	  	 	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci maiores quasi similique aliquam sint amet!
      		  	  	 </Card.Text>
      		  	  </Card.Body>
      		  </Card>
   		  </Col>

   		  <Col xs={12} md={4}>
      		  {/*3rd Card Component*/}	
      		  <Card className="p-3">
      		  	  <Card.Body>
      		  	  	 <Card.Title>Learn From Home</Card.Title>
      		  	  	 <Card.Text>
      		  	  	 	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci maiores quasi similique aliquam sint amet!
      		  	  	 </Card.Text>
      		  	  </Card.Body>
      		  </Card>
   		  </Col>
      </Row>
	);
};




